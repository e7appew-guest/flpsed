Source: flpsed
Section: graphics
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 11), libfltk1.3-dev
Build-Depends-Indep: inkscape
Homepage: http://flpsed.org/flpsed.html
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/debian/flpsed
Vcs-git: https://salsa.debian.org/debian/flpsed.git

Package: flpsed
Architecture: any
Depends: flpsed-data (>= ${source:Version}),
         ghostscript-x,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: xpdf-utils | poppler-utils
Description: WYSIWYG pseudo PostScript editor
 flpsed is a WYSIWYG pseudo PostScript editor. "Pseudo", because you can't
 remove or modify existing elements of a document. But flpsed lets you add
 arbitrary text lines to existing PostScript 1 documents. Added lines can
 later be reedited with flpsed.
 .
 Using pdftops, which is part of xpdf, one can convert PDF documents to
 PostScript and also add text to them. flpsed is useful for filling in forms,
 adding notes, etc.

Package: flpsed-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: flpsed (<< 0.7.3-4)
Replaces: flpsed (<< 0.7.3-4)
Description: WYSIWYG pseudo PostScript editor - data files
 flpsed is a WYSIWYG pseudo PostScript editor. "Pseudo", because you can't
 remove or modify existing elements of a document. But flpsed lets you add
 arbitrary text lines to existing PostScript 1 documents. Added lines can
 later be reedited with flpsed.
 .
 Using pdftops, which is part of xpdf, one can convert PDF documents to
 PostScript and also add text to them. flpsed is useful for filling in forms,
 adding notes, etc.
 .
 This package contains the common data files required by flpsed.
